# coding: UTF-8
########################################################################
# Copyright (C) 2024 Mark J. Blair, NF6X
#
# This file is part of nf6x_eetools.
#
# nf6x_eetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nf6x_eetools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with eetools.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Regression tests for drilltables module"""

import nf6x_eetools as ee

# Test cases to exercise corner conditions
# (diameter, margin, showdiam, expected)
drill_tests = (
    (0.1249, -1, False, 'no31'),
    (0.1249,  0, False, '1/8in'),
    (0.1249,  1, False, '1/8in'),
    (0.1251, -1, False, '1/8in'),
    (0.1251,  0, False, '1/8in'),
    (0.1251,  1, False, 'no30'),

    (0.1249, -1, True, 'no31 (0.120 in)'),
    (0.1249,  0, True, '1/8in (0.125 in)'),
    (0.1249,  1, True, '1/8in (0.125 in)'),
    (0.1251, -1, True, '1/8in (0.125 in)'),
    (0.1251,  0, True, '1/8in (0.125 in)'),
    (0.1251,  1, True, 'no30 (0.1285 in)'),
)

def test_drill():
    """Test ee.drill()"""
    for (diameter, margin, showdiam, exp) in drill_tests:
        assert ee.drill(diameter, margin, showdiam) == exp
