# coding: UTF-8
########################################################################
# Copyright (C) 2024 Mark J. Blair, NF6X
#
# This file is part of nf6x_eetools.
#
# nf6x_eetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nf6x_eetools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with eetools.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Regression tests for fractional module"""

import nf6x_eetools as ee

# Test cases to exercise corner conditions
# (value, denominator, rounding, correction, expected)
frac_tests = (
    (0.499, 64, -1, False, '31/64'),
    (0.499, 64,  0, False, '1/2'),
    (0.499, 64,  1, False, '1/2'),
    (0.501, 64, -1, False, '1/2'),
    (0.501, 64,  0, False, '1/2'),
    (0.501, 64,  1, False, '33/64'),

    (0.499, 64, -1, True, '31/64 +0.014625'),
    (0.499, 64,  0, True, '1/2 -0.001'),
    (0.499, 64,  1, True, '1/2 -0.001'),
    (0.501, 64, -1, True, '1/2 +0.001'),
    (0.501, 64,  0, True, '1/2 +0.001'),
    (0.501, 64,  1, True, '33/64 -0.014625'),

    (1.499, 64, -1, False, '1+31/64'),
    (1.499, 64,  0, False, '1+1/2'),
    (1.499, 64,  1, False, '1+1/2'),
    (1.501, 64, -1, False, '1+1/2'),
    (1.501, 64,  0, False, '1+1/2'),
    (1.501, 64,  1, False, '1+33/64'),

    (1.499, 64, -1, True, '1+31/64 +0.014625'),
    (1.499, 64,  0, True, '1+1/2 -0.001'),
    (1.499, 64,  1, True, '1+1/2 -0.001'),
    (1.501, 64, -1, True, '1+1/2 +0.001'),
    (1.501, 64,  0, True, '1+1/2 +0.001'),
    (1.501, 64,  1, True, '1+33/64 -0.014625'),
)


def test_frac():
    """Test ee.frac() corner conditions"""
    for (value, denominator, rounding, correction, exp) in frac_tests:
        assert ee.frac(value, denominator, rounding, correction) == exp
