# coding: UTF-8
########################################################################
# Copyright (C) 2024 Mark J. Blair, NF6X
#
# This file is part of nf6x_eetools.
#
# nf6x_eetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nf6x_eetools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with eetools.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Regression tests for eseries module"""

from decimal import Decimal
import pytest
import nf6x_eetools as ee

# Test cases to exercise corner conditions
# (series_class, value, expected)
eseries_tests = (
    # Basic approximation test with integer value that produces a
    # different approximation in each series
    (ee.E3,   16666, '22 kΩ ±40%'),
    (ee.E6,   16666, '15 kΩ ±20%'),
    (ee.E12,  16666, '18 kΩ ±10%'),
    (ee.E24,  16666, '16 kΩ ±5%'),
    (ee.E48,  16666, '16.9 kΩ ±2%'),
    (ee.E96,  16666, '16.5 kΩ ±1%'),
    (ee.E192, 16666, '16.7 kΩ ±0.5%'),

    # Basic approximation test with float value that produces a
    # different approximation in each series
    (ee.E3,   1.6666E4, '22 kΩ ±40%'),
    (ee.E6,   1.6666E4, '15 kΩ ±20%'),
    (ee.E12,  1.6666E4, '18 kΩ ±10%'),
    (ee.E24,  1.6666E4, '16 kΩ ±5%'),
    (ee.E48,  1.6666E4, '16.9 kΩ ±2%'),
    (ee.E96,  1.6666E4, '16.5 kΩ ±1%'),
    (ee.E192, 1.6666E4, '16.7 kΩ ±0.5%'),

    # Basic approximation test with str value that produces a
    # different approximation in each series
    (ee.E3,   '16666', '22 kΩ ±40%'),
    (ee.E6,   '16666', '15 kΩ ±20%'),
    (ee.E12,  '16666', '18 kΩ ±10%'),
    (ee.E24,  '16666', '16 kΩ ±5%'),
    (ee.E48,  '16666', '16.9 kΩ ±2%'),
    (ee.E96,  '16666', '16.5 kΩ ±1%'),
    (ee.E192, '16666', '16.7 kΩ ±0.5%'),

    # Test rounding down
    (ee.E96,  '19799.9', '19.6 kΩ ±1%'),
    (ee.E96,  '19800.0', '19.6 kΩ ±1%'),
    (ee.E96,  '19800.1', '20.0 kΩ ±1%'),

    # Test rounding at decade boundary
    (ee.E96,  '987', '976 Ω ±1%'),
    (ee.E96,  '988', '976 Ω ±1%'),
    (ee.E96,  '989', '1.00 kΩ ±1%'),

    # Test re-approximation
    (ee.E96,  ee.E24(16666), '16.5 kΩ ±1%'),
)


def test_approx():
    """Test approximation corner conditions"""
    for (sc, value, exp) in eseries_tests:
        r = sc(value)
        assert f'{str(r)} ±{r.tolerance():%}' == exp

def test_repr():
    """Test repr()"""
    assert repr(ee.E96(12345)) == 'E96(12345)'

def test_next():
    """Test next_plus(), next_minus() with rollovers at decade boundaries"""
    r = ee.E96(10E3)
    assert str(r.next_minus()) == '9.76 kΩ'
    assert str(r) == '10.0 kΩ'
    assert str(r.next_plus()) == '10.2 kΩ'
    r = ee.E96(9760)
    assert str(r.next_minus()) == '9.53 kΩ'
    assert str(r) == '9.76 kΩ'
    assert str(r.next_plus()) == '10.0 kΩ'
    # Test next_toward()
    r = ee.E96(9760)
    assert str(r.next_toward(9759)) == '9.53 kΩ'
    assert str(r.next_toward(9760)) == '9.76 kΩ'
    assert str(r.next_toward(9761)) == '10.0 kΩ'

def test_getters():
    """Test the attribute getters"""
    r = ee.E96(9761)
    assert r.orig() == 9761
    assert r.exact() == Decimal(9761)
    assert r.tolerance() == Decimal('0.01')
    assert f'{r.error():.4%}' == '-0.0102%'
    assert r.precision() == 3
    assert len(r.series()) == 96
    assert r.index() == 95

def test_warnings():
    """Test the deprecation warnings"""
    with pytest.deprecated_call():
        assert str(ee.E96(9760).prev()) == '9.53 kΩ'
    with pytest.deprecated_call():
        assert str(ee.E96(9760).next()) == '10.0 kΩ'
    with pytest.deprecated_call():
        (r1, r2, err) = ee.match_ratio(12345, 23456, 4)
        assert str(r1) == '11.3 kΩ'
        assert str(r2) == '21.5 kΩ'
        assert f'{err:.2%}' == '0.14%'

def test_exceptions():
    """Test exceptions"""
    with pytest.raises(ValueError):
        ee.E96(0)
    with pytest.raises(ValueError):
        ee.ratio(10,20,0)
    with pytest.raises(ValueError):
        ee.ratio(10,20,1.0)
    with pytest.raises(ValueError):
        ee.divider(5.0, 12.0)

def test_ratio():
    """Test ee.ratio()"""
    (r1, r2, err) = ee.ratio(12345, 23456)
    assert str(r1) == '12.4 kΩ'
    assert str(r2) == '23.7 kΩ'
    assert f'{err:.2%}' == '0.59%'
    (r1, r2, err) = ee.ratio(12345, 23456, 4)
    assert str(r1) == '11.3 kΩ'
    assert str(r2) == '21.5 kΩ'
    assert f'{err:.2%}' == '0.14%'
    (r1, r2, err) = ee.ratio(ee.E12(12345), ee.E12(23456))
    assert str(r1) == '12 kΩ'
    assert str(r2) == '22 kΩ'
    assert f'{err:.2%}' == '-3.64%'

def test_divider():
    """Test ee.divider()"""
    (r1, r2, err) = ee.divider(12.0, 5.0)
    assert str(r1) == '5.90 kΩ'
    assert str(r2) == '4.22 kΩ'
    assert f'{err:+.3%}' == '+0.079%'
    (r1, r2, err) = ee.divider(12.0, 5.0, 1000)
    assert str(r1) == '590 Ω'
    assert str(r2) == '422 Ω'
    assert f'{err:+.3%}' == '+0.079%'
    (r1, r2, err) = ee.divider(12.0, 5.0, ee.E24(100000))
    assert str(r1) == '56 kΩ'
    assert str(r2) == '39 kΩ'
    assert f'{err:+.3%}' == '-1.474%'
