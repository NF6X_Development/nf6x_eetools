# Version 1.0.1

* Comment out stray debug print()

# Version 1.0

* Major compatibility-breaking update
* Eseries classes now inherit from Decimal, allowing arithmetic operations
* Eseries classes now include units, defaulting to Ω
* Eseries.next() renamed to next_plus(), overriding existing Decimal method
* Eseries.prev() renamed to next_minus(), overriding existing Decimal method
* Function match_ratio() renamed to ratio()
* Added divider()
* Added pytest suite with 100% code coverage
* Passes pylint
* Fixed improper approximations at decade boundaries

# Version 0.3

* Clean up code for PEP8 compliance
* Replace old setup.py with pyproject.toml

# Version 0.2

* Add fractional dimension converter
* Add drill tables

# Version 0.1

* First release
